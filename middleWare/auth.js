var express = require('express')
var router = express.Router()
const Token = require("../models/token");
const User = require("../models/user");
const moment = require("moment");

module.exports = async (req, res, next) => {
  let response = {
    result: "Please provide token"
  }
  if (req.headers.usertoken) {
    const result = await Token.findOne({value: req.headers.usertoken}).exec();
    if (result) {
      const compareData = moment(result.expiredToken).isAfter(moment());
      console.log("compareData", compareData);
      if (compareData) {
        return next();
      }
      response.result = "Token is expired";
      res.status(403);
      return res.json(response);
    }
    response.result = "Token is invalid";
    res.status(403);
    return res.json(response);
  }
  res.status(400);
  return res.json(response);
};