const uuidv4 = require("uuidv4");
const express = require("express");
const app = express();
const session = require('express-session')
const User = require("../models/user");
const Token = require("../models/token");
const moment = require("moment");
const rootPage = async function(req, res) {
  await Token.deleteMany( { expiredToken : { $lt : moment() } } ).exec();
  res.render('index', { author: 'sean' });
};
const homePage = function(req, res) {
  res.render('index', { author: 'richie' });
};
const aboutPage = function(req, res) {
  res.render('index', { author: 'peter' });
};
const postData = function (req, res) {
  const data = {
    user_name: req.body.user_name,
    password: req.body.password
  };
  const user = new User(data);
  user.save()
    .then(user => {
      res.json('User added successfully');
    })
    .catch(err => {
      res.status(400).send("unable to save to database");
    });
};
const login = async function (req, res) {
  let response = {
    result: "User Name or Password is incorrect"
  }
  const requestData = {
    user_name: req.body.user_name,
    password: req.body.password
  }
  const result = await User.findOne({user_name: requestData.user_name, password: requestData.password}).exec();
  if (result) {
    const resultFindToken = await Token.findOne({userID: result._id}).exec();
    console.log("resultFindToken", resultFindToken);
    if (resultFindToken) {
      response.result = resultFindToken.value;
      res.json(response);
    } else {
      const userToken = uuidv4();
      const expiredToken = moment().add(1, 'hours');
      const tokenDB = new Token({value: userToken, userID: result._id, expiredToken});
      tokenDB.save();
      response.result = userToken;
      res.json(response);
    }
  }
  res.status(400);
  res.json(response);
};

const resMe = async function (req, res) {
  console.log("calling resme");
  const result = await Token.findOne({value: req.headers.usertoken}).exec();
  const userResponse = await User.findOne({_id: result.userID});
  console.log("userResponse", userResponse);
  res.json(userResponse);
}
module.exports = { rootPage, homePage, aboutPage, postData, login, resMe }