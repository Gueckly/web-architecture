import React from 'react';
import ReactDOM from 'react-dom';
import UserForm from "./UserForm";

export default {
  widgets: {
    render: (args, options) => {
      ReactDOM.render(
          <UserForm options={options}/>, args
      );
    }
  }
};
