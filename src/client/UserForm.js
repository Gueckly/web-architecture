import React from "react";
import PropTypes from "prop-types";
import { postRequest, getRequest, login } from "./utils/request";
import UserFormStyle from "./styles/_userForm.scss";

// const UserFormStyle = {
//   loginContainer: {
//     display: "flex",
//     justifyContent: "center",
//     alignItems: "center",
//     flexDirection: "column",
//     maxWidth: "400px",
//     margin: "0 auto"
//   },
//   inputStyle: {
//     width: "100%",
//     padding: "10px",
//     fontSize: "16px"
//   },
//   buttonStyle: {
//     width: "100%",
//     padding: "15px",
//     fontSize: "16px",
//     borderRadius: "10px",
//     cursor: "pointer",
//     outline: "none"
//   },
//   mainContainer: {
//     height: "100vh",
//     display: "flex",
//     width: "100%",
//   }
// }
class UserForm extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      name: "",
      password: "",
      isLoginSuccess: false
    };
    this.handleChange = this.handleChange.bind(this);
    this.login = this.login.bind(this);
  }
  componentDidMount() {
    console.log("this.props.options", this.props.options);
  }
  handleChange(e) {
    const name = e.target.name;
    const value = e.target.value;
    this.setState({
      [name]: value
    }, () => {
      console.log("this.state", this.state);
    })
  }
  async login() {
    console.log("login", this.state);
    const data = {
      user_name: this.state.name,
      password: this.state.password
    }
    const result = await login(data); //LOGIN
    console.log("resulttt", result);
    if(result){
      this.props.options.onSuccess(result.data.result);
      const getMe = await getRequest("me", result.data.result);
      this.setState({
        isLoginSuccess: getMe ? true: false
      });
    }
  }
  render() {
    const { name, isLoginSuccess } = this.state;
    return (
      <div className={UserFormStyle.mainContainer}>
        {isLoginSuccess ?
          <p>Hello {name}</p>
          :
          <div className={UserFormStyle.loginContainer}>
            <input className={UserFormStyle.inputStyle} placeholder="UserName" name="name" value={this.state.name} onChange={this.handleChange} />
            <br />
            <input className={UserFormStyle.inputStyle} placeholder="Password" name="password" value={this.state.password} onChange={this.handleChange} />
            <br />
            <button className={UserFormStyle.buttonStyle} onClick={this.login}>Press</button>
          </div>
        }
      </div>
    );
  }
}

UserForm.defaultProps = {
  options: {
    onSuccess: () => {}
  }
}
export default UserForm;
