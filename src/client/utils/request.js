import axios from "axios";
const headers = {
  Accept: "application/json",
  "Content-Type": "application/json"
};
const requestFetch = async (url, method, userTokenSession, bodyPayload) => {
  const finalURL = `/${url}`;
  headers.userToken = userTokenSession;
  // const actualHeaders = headerParams || headers;
  let requestOption = { method, headers: headers };
  if (method !== "get") {
    if (headers["Content-Type"] === "application/json") {
      requestOption.body = JSON.stringify(bodyPayload);
    } else {
      requestOption.body = bodyPayload;
    }
  }
  return await axios(finalURL, requestOption);
};
const getRequest = async (url, userTokenSession) => {
  return await requestFetch(url, "get", userTokenSession);
};
const postRequest = async (url, userTokenSession, data ) => {
  return await requestFetch(url, "post", userTokenSession, data);
};
const login = async data => {
  return axios.post('/login', data);
};

export { getRequest, postRequest, login };