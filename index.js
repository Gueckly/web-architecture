const express = require('express')
const app = express();
const cors = require('cors');
const session = require('express-session');
const isDev = app.get('env') === 'development';
const webpackDevMiddleware = require('webpack-dev-middleware');
const config = require('./webpack.config.js');

// connect mongoDB

const mongoose = require('mongoose');
const bodyParser = require('body-parser');
var mongoDB = 'mongodb://root:123456as@ds149711.mlab.com:49711/web_richie';
mongoose.connect(mongoDB, { useNewUrlParser: true });


const webpack = require("webpack");
const compiler = webpack(config);


//Get the default connection
var db = mongoose.connection;

//Bind connection to error event (to get notification of connection errors)
db.on('error', console.error.bind(console, 'MongoDB connection error:'));
app.use(cors())
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

//Config nunjucks 
const expressNunjucks = require('express-nunjucks');
app.set('views', __dirname + '/views');
app.set('view engine', "html");

expressNunjucks(app, {
    watch: isDev,
    noCache: isDev
});
app.use(webpackDevMiddleware(compiler, {
    publicPath: config.output.publicPath
  }))

// config using route from folder route
const routes = require('./routes/route');
const port = 3000;
app.use('/', routes)

app.listen(port, () => console.log(`Example app listening on port ${port}!`))