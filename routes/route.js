var express = require('express')
var router = express.Router()
const controllers = require('../controllers/index')
const authication = require('../middleWare/auth');

router.post('/login', controllers.login)
router.get('/me', authication, controllers.resMe);

router.get('/', controllers.rootPage)
// define the home page route
router.post('/postUser', controllers.postData)

module.exports = router